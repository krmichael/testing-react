import React from "react";
import { shallow } from "enzyme";
import Counter from "../src/Counter";

describe("Testing Counter Component", () => {
  it("sholud render correctly", () => {
    const wrapper = shallow(<Counter counter={5} />);

    expect(wrapper.debug()).toMatchSnapshot();
    wrapper.setProps({ counter: 10 });
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
