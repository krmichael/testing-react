import React from "react";
import { shallow } from "enzyme";
import configureStore from "redux-mock-store";

import Todos from "../src/Todos";

const mockStore = configureStore();

const INITIAL_STATE = {
  todos: [
    { id: 1, label: "Fazer café" },
    { id: 2, label: "Aprender Jest" },
    { id: 3, label: "Aprender Nodejs" }
  ]
};

const store = mockStore(INITIAL_STATE);

describe("Testing Todos Component", () => {
  it("should render correctly", () => {
    const wrapper = shallow(<Todos />, { context: { store } }).dive();
    //downgrade react-redux for react-redux@5.1.1 (dive() not working with actual version...)

    expect(wrapper.debug()).toMatchSnapshot();
  });
});
