import React from "react";

export default function App({ counter }) {
  return (
    <div>
      <h2>Você tem {counter} counter.</h2>
    </div>
  );
}
