import React from "react";
import { connect } from "react-redux";

function Todos({ todos }) {
  return (
    <ul className="todos__app">
      {todos.map(todo => (
        <li key={todo.id}>{todo.label}</li>
      ))}
    </ul>
  );
}

const mapStateToProps = state => ({
  todos: state.todos
});

export default connect(mapStateToProps)(Todos);
